
const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

var app = express();
/*
 Using hbs as the default view engine requires just one line of code in your app setup. 
This will render .hbs files when res.render is called. 
*/
app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials');
// This will use the static html page
app.use(express.static(__dirname + '/public'));


app.use((req, res, next) => {
    var now = new Date().toString();
    var log = `Time: ${now} Method: ${req.method} URL: ${req.url}`;
    console.log(log);

    fs.appendFile('server.log', log + '\n', (error) => {
        if(error){
            console.log('Unable to append to server.log file');
        }
    });
    next();
});
var chayObject = {
    status: 'Success',
    data: {
        name: 'Sai Chaitanya Nare',
        likes: [
            'Biking',
            'Gym',
            'Music'
        ]

    }
}

app.get('/', (req, res) => {

    // res.send('<h1>Hello Express!</h1>');
    res.render('home.hbs', {
        pageTitle: 'Home Page',
        message: 'Welcome to Home Page',
        currentYear: new Date().getFullYear()
    });
});

app.get('/about', (req, res) => {

    // res.send(chayObject);
    res.render('about.hbs', {
        pageTitle: 'About Page',
        currentYear: new Date().getFullYear()
    });
});

app.get('/bad', (req, res) => {


    res.send({
        errorMessage: 'Unable to handle request'
    });
});


app.listen(3000, () => {
    console.log('Server is up on 3000 port');
});